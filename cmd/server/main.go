package main

import (
	"context"
	"os"
	"os/signal"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/gocloud9/eventhub"
)

var (
	name      = "eventhub"
	version   = "v1.0.0"
	buildTime = time.Now().Format(time.RFC3339)
)

func main() {
	service, err := eventhub.NewService(&eventhub.ServiceInfo{
		Name:      name,
		Version:   version,
		BuildTime: buildTime,
	})
	if err != nil {
		logrus.WithError(err).Error("failed to create service")
		os.Exit(1)
	}

	ctx, cancel := context.WithCancel(context.Background())
	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, os.Interrupt)

	go func() {
		<-signalCh
		cancel()
	}()

	service.Serve(ctx)
}
