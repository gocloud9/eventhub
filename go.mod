module gitlab.com/gocloud9/eventhub

go 1.14

require (
	github.com/caarlos0/env/v6 v6.3.0
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.3.3
	github.com/google/uuid v1.1.1
	github.com/sirupsen/logrus v1.6.0
	github.com/streadway/amqp v1.0.0
	gitlab.com/o5slab/mcore v1.1.1
	google.golang.org/grpc v1.31.0
	gopkg.in/yaml.v2 v2.2.8
)
