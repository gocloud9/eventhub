package eventhub

import (
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/gocloud9/eventhub/mq"
	pb "gitlab.com/gocloud9/eventhub/proto/eventhub"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *Service) publishMessage(req *pb.Event) error {
	if req.GetBody() == "" || req.GetTopic() == "" {
		return status.New(codes.Unavailable, "bad request").Err()
	}

	ctxlog := logrus.WithField("func", "Service.publishMessage")

	message := amqp.Publishing{
		Headers:     amqp.Table{},
		ContentType: "application/json",
		Body:        []byte(req.GetBody()),
	}
	if req.GetId() == "" {
		req.Id = uuid.New().String()
	}
	message.Headers["x-request-id"] = req.GetId()
	if err := mq.DefaultPublisher.Publish(req.GetTopic(), message); err != nil {
		ctxlog.WithError(err).Error("unable to publish message to broker")
		return status.New(codes.Internal, "internal error").Err()
	}

	ctxlog.
		WithField("message.body", req.GetBody()).
		WithField("message.id", req.GetId()).
		WithField("message.topic", req.GetTopic()).
		Debug("published message")

	return nil
}
