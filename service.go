package eventhub

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/gocloud9/eventhub/mq"
	"google.golang.org/grpc"
)

// Service presents a service that controls the whole, listens on http, grpc, etc.
// - Init(): do all initialization, should return error if any start up failed
// - Serve(ctx): start the service and block the process until context is done
type Service struct {
	info *ServiceInfo

	config *Config

	httpserver *http.Server
	ginengine  *gin.Engine
	grpcserver *grpc.Server
	wg         sync.WaitGroup

	handlersCfg HandlersConfig
	handlers    []*serviceHandler
}

// NewService init a new service
func NewService(info *ServiceInfo) (*Service, error) {
	if strings.HasPrefix(info.BasePath, "/") {
		info.BasePath = "/" + info.BasePath
	}
	s := &Service{
		info:   info,
		config: loadConfigEnv(),
	}
	s.info.BasePath = s.config.BasePath

	// load serviceHandler config
	if f, err := os.Open(s.config.LConfigPath); err != nil {
		return nil, fmt.Errorf("unable to open handlers config file: %s", err.Error())
	} else {
		if s.handlersCfg, err = loadHandlersConfig(f); err != nil {
			return nil, fmt.Errorf("unable to parse handlers config: %s", err.Error())
		}
		_ = f.Close()
	}

	s.registerServicesHandlers()
	s.registerHttpHandlers()

	// make sure we can connect broker
	mq.MustOpenDefault()

	return s, nil
}

// Serve start serving request
func (s *Service) Serve(ctx context.Context) {
	s.wg.Add(1)

	go func() {
		s.serveHttp()
		s.wg.Done()
	}()

	s.wg.Add(1)
	go func() {
		s.serveGrpc()
		s.wg.Done()
	}()

	s.subscribeListeners()
	s.wg.Add(1)
	go func() {
		mq.DefaultConsumer.Start(ctx)
		s.wg.Done()
	}()

	<-ctx.Done()
	sdctx, sdcancel := context.WithTimeout(ctx, time.Second*3)
	defer sdcancel()
	_ = s.httpserver.Shutdown(sdctx)
	s.grpcserver.Stop()

	s.wg.Wait()
}
