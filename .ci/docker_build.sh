#!/usr/bin/env sh

set -e

export image="${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}"
export hash_tag=$(git rev-parse --short HEAD)
echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
if docker pull "${CI_REGISTRY_IMAGE}:${hash_tag}"; then
    docker tag "${CI_REGISTRY_IMAGE}:${hash_tag}" $image
    docker push $image
    exit 0
fi

export build_arg="--build-arg VERSION=${CI_COMMIT_REF_NAME} --build-arg BUILD_TIME=$( date '+%FT%T.%1N%:z') --build-arg NAME=${SERVICE}"
docker build -t ${image} ${build_arg} .
docker tag ${image} "${CI_REGISTRY_IMAGE}:${hash_tag}"
docker push ${image}
docker push "${CI_REGISTRY_IMAGE}:${hash_tag}"
