package eventhub

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/gocloud9/eventhub/mq"
	pb "gitlab.com/gocloud9/eventhub/proto/eventhub"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

// httpclient service client that sends request to target service
type httpclient interface {
	Do(r *http.Request) (*http.Response, error)
}

type serviceHandler struct {
	config    *ServiceHandlerConfig
	service   string
	url       string
	onmessage func(e *pb.Event) error
	maxtries  int
	client    httpclient

	conn    *grpc.ClientConn
	gclient pb.EventHubClient
}

func (h *serviceHandler) Handle(m amqp.Delivery) {

	ctxlog := logrus.WithField("func", "serviceHandler.Handle")

	// build event
	e := &pb.Event{
		Topic: m.RoutingKey,
		Body:  string(m.Body),
	}

	var rid string
	if v, ok := m.Headers["x-request-id"]; ok {
		rid = v.(string)
	} else {
		rid = uuid.New().String()
	}
	e.Id = rid

	ctxlog = ctxlog.WithField("rid", rid)
	for i := 0; i < h.maxtries; i++ {
		if err := h.onmessage(e); err == nil {
			break
		} else {
			msg := "failed to deliver message, "
			if i < h.maxtries {
				msg += "retry ..."
			} else {
				msg += "give up."
			}
			ctxlog.WithError(err).WithField("try", i).Error(msg)
		}

		// sleep 2 seconds before retry
		time.Sleep(time.Second * 2)
	}
}

func (h *serviceHandler) handleHttp(e *pb.Event) error {
	ctxlog := logrus.WithField("func", "serviceHandler.handleHttp")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	payloadBytes, err := json.Marshal(e)

	if err != nil {
		ctxlog.WithError(err).Error("unable to marshal payload")
		return nil
	}

	ctxlog = ctxlog.
		WithField("request.id", e.GetId()).
		WithField("request.payload", string(payloadBytes)).
		WithField("request.url", h.url)

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, h.url, bytes.NewReader(payloadBytes))
	if err != nil {
		ctxlog.WithError(err).Error("unable to create request")
		return nil
	}
	req.Header.Set("x-request-id", e.GetId())
	req.Header.Set("User-Agent", "eventhub")
	req.Header.Set("Content-Type", "application/json")
	resp, err := h.client.Do(req)
	if err != nil {
		ctxlog.WithError(err).Error("error while calling service")
		return err
	}

	body, err := ioutil.ReadAll(resp.Body)
	_ = resp.Body.Close()
	if err != nil {
		ctxlog.WithError(err).Error("failed to read response body")
	}

	ctxlog = ctxlog.WithField("response.code", resp.StatusCode).WithField("response.body", string(body))
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		ctxlog.Error("failed to deliver message")
		return fmt.Errorf("invalid response code: %d", resp.StatusCode)
	}

	ctxlog.Debug("delivered message")

	return nil
}

func (h *serviceHandler) handleGrpc(e *pb.Event) error {
	ctxlog := logrus.WithField("func", "serviceHandler.handleGrpc")
	ctxlog.
		WithField("payload", e).
		WithField("target_service", h.service).
		Debug("delivering message")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	if _, err := h.gclient.Emit(ctx, e); err != nil {
		return err
	}

	return nil
}

func newHandler(service string, config *ServiceHandlerConfig) *serviceHandler {
	ctxlog := logrus.WithField("func", "newHandler")
	l := &serviceHandler{
		service:  service,
		config:   config,
		maxtries: 3,
	}

	if strings.HasPrefix(l.config.Url, "http") {
		l.url = l.config.Url
		l.onmessage = l.handleHttp
		l.client = &http.Client{}
	} else {
		l.url = strings.TrimPrefix(l.config.Url, "tcp://")
		l.onmessage = l.handleGrpc

		kacp := keepalive.ClientParameters{
			Time:                10 * time.Second,
			Timeout:             time.Second,
			PermitWithoutStream: true,
		}
		var err error
		l.conn, err = grpc.Dial(l.url, grpc.WithInsecure(), grpc.WithKeepaliveParams(kacp))
		if err != nil {
			ctxlog.WithError(err).WithField("url", l.url).Error("unable to dial gRPC")
			return nil
		}
		l.gclient = pb.NewEventHubClient(l.conn)
	}

	return l
}

func (s *Service) registerServicesHandlers() {
	s.handlers = []*serviceHandler{}
	for service, handlersConfig := range s.handlersCfg {
		l := newHandler(service, handlersConfig)

		s.handlers = append(s.handlers, l)
	}
}

// subscribeListeners subscribes handlers to broker to listen events
func (s *Service) subscribeListeners() {

	ctxlog := logrus.WithField("func", "Service.subscribeListeners")
	ctxlog.WithField("total_handlers", len(s.handlers)).Debug("start registering handlers")

	for _, h := range s.handlers {
		for _, l := range h.config.Listeners {
			ctxlog.WithField("total_listeners", len(h.config.Listeners)).Debug("register listeners")
			// create consumer tag
			tag := l.Queue
			if l.Queue == "" {
				tag = uuid.New().String()
			}
			l.tag = h.service + ":" + tag
			ctxlog.WithField("tag", l.tag).Debug("registering handler")

			for _, topic := range l.Topics {
				topicTag := fmt.Sprintf("%s:%s-%s", l.tag, topic, uuid.New().String())

				// subscribe to broker
				queueListener := &mq.MsgListener{
					Topic:   topic,
					Queue:   l.Queue,
					Handler: h.Handle,
					Tag:     topicTag,
				}
				ctxlog = ctxlog.WithField("topicTag", topicTag)

				if err := mq.DefaultConsumer.AddListener(queueListener); err != nil {
					ctxlog.WithError(err).Error("failed to consume")
					continue
				}

				ctxlog.Debug("registered")
			}
		}
	}
}
