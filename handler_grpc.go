package eventhub

import (
	"context"
	"fmt"
	"net"

	"github.com/sirupsen/logrus"
	pb "gitlab.com/gocloud9/eventhub/proto/eventhub"
	"google.golang.org/grpc"
)

//go:generate protoc -I. --go_out=plugins=grpc:. --go_opt=paths=source_relative ./proto/eventhub/eventhub.proto

// Emit ...
func (s *Service) Emit(_ context.Context, req *pb.Event) (*pb.EmitEventResponse, error) {
	if err := s.publishMessage(req); err != nil {
		return nil, err
	}

	return &pb.EmitEventResponse{}, nil
}

func (s *Service) serveGrpc() {
	ctxlog := logrus.WithField("func", "Service.serveGrpc")

	s.grpcserver = grpc.NewServer()
	pb.RegisterEventHubServer(s.grpcserver, s)

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", s.config.GrpcPort))
	if err != nil {
		ctxlog.WithError(err).Error("failed to listen")
		return
	}
	ctxlog.WithField("addr", l.Addr().String()).Info("start listening")
	if err := s.grpcserver.Serve(l); err != nil {
		ctxlog.WithError(err).Error("failed to serve gRPC")
	}
}
