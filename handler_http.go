package eventhub

import (
	"fmt"
	"net"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "gitlab.com/gocloud9/eventhub/proto/eventhub"
	"gitlab.com/o5slab/mcore/gina"
)

type customWriter struct{}

func (customWriter) Write(data []byte) (int, error) {
	logrus.Debugf("%s", data)
	return len(data), nil
}

func (s *Service) registerHttpHandlers() {

	gin.DefaultWriter = customWriter{}
	s.ginengine = gin.New()

	s.ginengine.Use(
		gina.NiceFailureRecovery(),
		gina.RequestID("x-request-id"),
		gina.RequestLog(s.info.Name),
	)
	router := s.ginengine.Group(s.info.BasePath)
	router.GET("", gina.NewHealthHandler(s.info.Name, s.info.Version, s.info.BuildTime))
	router.POST("/events", s.httpEmit)

	s.httpserver = &http.Server{
		Handler: s.ginengine,
	}
}

func (s *Service) serveHttp() {
	ctxlog := logrus.WithField("func", "Service.serveHttp")

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", s.config.Port))
	if err != nil {
		ctxlog.WithError(err).Error("failed to listen")
		return
	}

	ctxlog.WithField("addr", l.Addr().String()).Info("start listening")
	if err := s.httpserver.Serve(l); err != nil && err != http.ErrServerClosed {
		ctxlog.WithError(err).Error("failed to serve")
	}
}

func (s *Service) httpEmit(c *gin.Context) {
	req := &pb.Event{}
	if err := c.ShouldBindJSON(&req); err != nil {
		panic(err)
	}

	if req.GetId() == "" {
		req.Id = c.GetString("x-request-id")
	}

	if err := s.publishMessage(req); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	c.Status(http.StatusCreated)
}
