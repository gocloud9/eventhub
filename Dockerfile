FROM golang:1.14

ARG VERSION=0.0.0
ARG NAME="hub"
ARG BUILD_TIME="?"

ENV CGO_ENABLED=0 GO111MODULE=on

WORKDIR /src
ADD go.mod /src
ADD go.sum /src

# cache go modules first
RUN go mod download

ADD . /src

RUN go build -ldflags="-s -w -X main.name=${NAME} -X main.version=${VERSION} -X main.buildTime=${BUILD_TIME}" -o /app ./cmd/server

FROM alpine:3.11
COPY --from=0 /app /usr/bin/app
ADD ./config.yml /app/
WORKDIR /app

CMD ["/usr/bin/app"]
