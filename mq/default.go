package mq

import "os"

var (
	// DefaultExchange default exchange name
	DefaultExchange = "events"

	DefaultQueueUrl  = ""
	DefaultConsumer  MsgQ
	DefaultPublisher MsgQ
)

// MustOpenDefault initialize default publisher & consumer
// try to connect publisher as testing the connection
func MustOpenDefault() {
	if DefaultQueueUrl == "" {
		if DefaultQueueUrl = os.Getenv("AMQP_URL"); DefaultQueueUrl == "" {
			panic("empty queue URL, double check AMQP_URL env")
		}
	}

	DefaultPublisher = NewMsgQ(DefaultQueueUrl)
	//DefaultConsumer = NewMsgQ(DefaultQueueUrl)
	DefaultConsumer = DefaultPublisher

	if err := DefaultPublisher.Connect(); err != nil {
		panic(err)
	}
}
