package eventhub

import (
	"io"
	"io/ioutil"
	"os"
	"time"

	"github.com/caarlos0/env/v6"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

func init() {
	// init logger
	if l, err := logrus.ParseLevel(os.Getenv("LOG_LEVEL")); err == nil {
		logrus.SetLevel(l)
	}
	if os.Getenv("LOG_FORMAT") != "text" {
		logrus.SetFormatter(&logrus.JSONFormatter{TimestampFormat: time.RFC3339})
	}
	logrus.SetOutput(os.Stdout)
}

// Config presents an application configuration
type Config struct {
	Port        int    `env:"PORT" envDefault:"8080"`
	GrpcPort    int    `env:"GRPC_PORT" envDefault:"8081"`
	AmqpUrl     string `env:"AMQP_URL" envDefault:"amqp://guest:guest@rabbitmq:5672/"`
	LConfigPath string `env:"LISTENER_CONFIG_PATH" envDefault:"config.yml"`
	BasePath    string `env:"BASE_PATH" envDefault:"hub"`
}

// loadConfigEnv loads environment to config
func loadConfigEnv() *Config {
	config := Config{}
	if err := env.Parse(&config); err != nil {
		panic(err)
	}
	return &config
}

// ServiceInfo presents the information of current running service, it's helpful for health check
type ServiceInfo struct {
	Name      string
	Version   string
	BuildTime string
	BasePath  string
}

type ServiceListenerConfig struct {
	Topics []string `yaml:"topics"`
	Queue  string   `yaml:"queue"`

	tag string
}

// ServiceHandlerConfig presents a serviceHandler config
type ServiceHandlerConfig struct {
	Url       string                   `yaml:"url"`
	Listeners []*ServiceListenerConfig `yaml:"listeners"`
}

// HandlersConfig presents a list of handlers config
// the map key should identify itself
type HandlersConfig map[string]*ServiceHandlerConfig

func loadHandlersConfig(configReader io.Reader) (HandlersConfig, error) {
	configBytes, err := ioutil.ReadAll(configReader)
	if err != nil {
		return nil, err
	}
	config := make(HandlersConfig)
	if err = yaml.Unmarshal(configBytes, &config); err != nil {
		return nil, err
	}
	return config, nil
}
