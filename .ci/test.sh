#!/bin/bash

# this script performs the test stage
# - run linting
# - run unit tests

# should exit on any failed
set -e

if ! command -v golangci-lint &> /dev/null; then
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.30.0
fi

golangci-lint run -v
go test ./... -v -cover
